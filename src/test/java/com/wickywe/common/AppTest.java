package com.wickywe.common;

import static org.junit.Assert.assertTrue;

import com.wickywe.common.Lottery.Lottery;
import com.wickywe.common.Lottery.LotteryItem;
import com.wickywe.common.Lottery.LotteryUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }


    public static void main(String[] args) {
        List<LotteryItem> lotteryItemList = new ArrayList<LotteryItem>();
        LotteryItem awardItem1 = new LotteryItem();
        awardItem1.setAwardName("红包10元");
        awardItem1.setAwardProbability(0.4D);
        lotteryItemList.add(awardItem1);

        LotteryItem awardItem2 = new LotteryItem();
        awardItem2.setAwardName("红包20元");
        awardItem2.setAwardProbability(0.025D);
        lotteryItemList.add(awardItem2);

        LotteryItem awardItem3 = new LotteryItem();
        awardItem3.setAwardName("谢谢参与");
        awardItem3.setAwardProbability(0.575D);
        lotteryItemList.add(awardItem3);

        Lottery lottery = new Lottery();
        LotteryUtils.calAwardProbability(lottery, lotteryItemList);
        System.out.println("抽奖活动中奖数字范围：["+lottery.getWinningStartCode()+","+lottery.getWinningEndCode()+")");
        LotteryUtils.beginLottery(lottery, lotteryItemList);
        for (LotteryItem item : lotteryItemList) {
            System.out.println(item.getAwardName()+" 中奖数字范围：["+item.getAwardStartCode()+","+item.getAwardEndCode()+"]");
        }
        System.out.println("以下是模拟的抽奖中奖结果：");
        LotteryItem award1 = LotteryUtils.beginLottery(lottery, lotteryItemList);
        if(award1==null){
            System.out.println("没有中奖");
        }
        System.out.println("抽中的数字是："+award1.getAwardCode()+",恭喜中奖："+award1.getAwardName()+",数字落点["+award1.getAwardStartCode()+","+award1.getAwardEndCode()+"]");
//        LotteryItem award2 = LotteryUtils.beginLottery(lottery, lotteryItemList);
//        System.out.println("抽中的数字是："+award2.getAwardCode()+",恭喜中奖："+award2.getAwardName()+",数字落点["+award2.getAwardStartCode()+","+award2.getAwardEndCode()+"]");
//        LotteryItem award3 = LotteryUtils.beginLottery(lottery, lotteryItemList);
//        System.out.println("抽中的数字是："+award3.getAwardCode()+",恭喜中奖："+award3.getAwardName()+",数字落点["+award3.getAwardStartCode()+","+award3.getAwardEndCode()+"]");
//        LotteryItem award4 = LotteryUtils.beginLottery(lottery, lotteryItemList);
//        System.out.println("抽中的数字是："+award4.getAwardCode()+",恭喜中奖："+award4.getAwardName()+",数字落点["+award4.getAwardStartCode()+","+award4.getAwardEndCode()+"]");
    }

}
