package com.wickywe.common.Cache;

import com.google.common.cache.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *  guava 缓存移除 异步通知
 */
public class TestCacheAsyncMgr {


    public static void main(String[] args) {

        RemovalListener<Integer, Map<Integer, Integer>> myRemoveLister = RemovalListeners.asynchronous(new MyRemovalListener(), Executors.newSingleThreadExecutor());

        LoadingCache<Integer, Map<Integer,Integer>> cache = CacheBuilder.newBuilder().
                    concurrencyLevel(8).
                    expireAfterAccess(30, TimeUnit.SECONDS).
                    initialCapacity(10).
                    maximumSize((long) 30).
                    recordStats().
                    removalListener(myRemoveLister).
                    build(new CacheLoader<Integer, Map<Integer,Integer>>() {
                        @Override
                        public Map<Integer,Integer> load(Integer key){

                            Map<Integer,Integer> map =new HashMap<>();
                            for (int i = 0; i < 10; i++) {
                                Random random = new Random();
                                map.put(i+key,random.nextInt(10+i));
                            }
                            System.out.println("@@@>>> load刷新"+map);
                            return map;
                        }
                    });

        try {
            for (int i = 0; i < 10; i++) {
                int playerId = 950101 + i;
                System.out.println(cache.get(playerId));
                System.out.println(cache.getIfPresent(playerId));
                System.out.println("size = "+cache.size());
                System.out.println("--------------");
            }
            CacheStats cs = cache.stats();
            cache.invalidate(950101);
            System.out.println(cs);
        }catch (Exception e){

        }

    }

    // 创建一个监听器
    private static class MyRemovalListener implements RemovalListener<Integer, Map<Integer,Integer>> {
        @Override
        public void onRemoval(RemovalNotification<Integer, Map<Integer,Integer>> notification) {
            String tips = String.format("key=%s,value=%s,reason=%s", notification.getKey(), notification.getValue(), notification.getCause());
            System.out.println(tips);

            try {
                // 模拟耗时
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            throw new RuntimeException();
        }
    }


}
