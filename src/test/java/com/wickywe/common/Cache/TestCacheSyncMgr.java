package com.wickywe.common.Cache;

import com.google.common.cache.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *  guava 缓存移除 同步通知
 */
public class TestCacheSyncMgr {


    public static void main(String[] args) {

        LoadingCache<Integer, Integer> cache = CacheBuilder.newBuilder().
                    concurrencyLevel(8).
                    expireAfterAccess(60, TimeUnit.SECONDS).
                    initialCapacity(10).
                    maximumSize((long) 30).
                    recordStats().
                    removalListener(new RemovalListener<Integer, Integer>() {
                                                        //                        @Override
                        public void onRemoval(RemovalNotification<Integer,  Integer> notification) {
                            System.out.println("缓存移除同步通知,key:"+notification.getKey()+"被移除 "+notification.getCause());
                            try {
                                // 模拟耗时
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).build(new CacheLoader<Integer, Integer>() {
                        @Override
                        public Integer load(Integer key){
                            return key;
                        }
                    });


        try {
            for (int i = 0; i < 50; i++) {
                int playerId = 950101 + i;
                System.out.println(cache.get(playerId));
                System.out.println(cache.getIfPresent(playerId));
                System.out.println("size = "+cache.size());
                System.out.println("--------------");
            }
            System.out.println("after: "+cache.size());
            CacheStats cs = cache.stats();
            System.out.println(cs+"--"+cs.hitRate() +","+cs.missRate());

        }catch (Exception e){

        }

    }

}
