package com.wickywe.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间工具类
 */
public class TimeUtils {
    private static String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
    /**
     * 当天的零点零分零秒的毫秒数（不受时区影响）
     * @return
     */
	public static long getTodayZore(){
		long current=System.currentTimeMillis();//当前时间毫秒数
		long zero=current/(1000L*3600*24)*(1000L*3600*24)- TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
		return  zero;
	}

    /**
     * 字符串yyyy-MM-dd HH:mm:ss格式的时间，转换成Date
     * @param strDate
     * @return
     */
    public static Date getDateByYyyyMMddHHmmss(String strDate){
        SimpleDateFormat sdf = new SimpleDateFormat (yyyyMMddHHmmss);
        try {
            return sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(TimeZone.getDefault().getRawOffset());
        System.out.println(getTodayZore());
    }
}
