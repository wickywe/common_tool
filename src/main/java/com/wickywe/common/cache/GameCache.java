package com.wickywe.common.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public abstract class GameCache<K,V> {
    protected static Logger logger = LoggerFactory.getLogger(GameCache.class);
    protected final LoadingCache<K, V> cache;


    protected GameCache(CacheLoader<K, V> loader, int initSize, int maxSize) {
        /**
         * 参数 走配置
         */
        this.cache = CacheBuilder.newBuilder().
                concurrencyLevel(8).
                expireAfterAccess(120L, TimeUnit.SECONDS).
                initialCapacity(initSize).
                maximumSize((long) maxSize).
                recordStats().
                build(loader);
    }


    public V get(K key) {
        if (key == null) {
            return null;
        } else {
            try {
                return this.cache.get(key);
            } catch (ExecutionException var4) {
                throw new RuntimeException(var4);
            }
        }
    }

    public void remove(K key) {
        this.cache.invalidate(key);
    }

    public void put(K key, V value) {
        this.cache.put(key, value);
    }
}
